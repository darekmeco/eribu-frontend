"use strict";
import Vue from 'vue';
import notification from 'ant-design-vue';
const Notification = {};
Notification.install = function(Vue) {
  Vue.prototype.$notification = notification;
 };
Vue.use(Notification);