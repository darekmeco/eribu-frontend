import Vue from 'vue';
import '@/config';
import '@/plugins/axios';
import '@/plugins/fs';
import '@/plugins/slugify';
import '@/plugins/message';
import '@/plugins/notification';
import App from './App.vue';
import router from './router';
import store from './store';
import 'ant-design-vue/dist/antd.css';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
