import {
    Component,
    Mixins,
} from 'vue-property-decorator';

import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import { Category } from '../../models/Category';
import _merge from 'lodash/merge';
import SlugUnique from '@/libs/mixins/SlugUnique';

@Component({
    template: require("./Edit.html"),
    components: {
        'a-editor': Editor,
    },
})
export default class Edit extends Mixins( FormMixin, SlugUnique) {

    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
    };

    public getFormData() {
        this.$axios.get(`${process.env.VUE_APP_APIURL}/blog/categories/edit`, {
            params: {
                id: this.$route.params.id,
            },
        }).then((res: any) => {
            this.setDataFields(this.form.getFieldsValue(), res.data.model);
        });
    }
    public submit() {
        this.form.validateFieldsAndScroll(
            (err: any, form: Category) => {
                if (!err) {
                    form = _merge(form, { _id: this.$route.params.id });
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/blog/categories/update`, form)
                    .then((response: AxiosResponse) => {
                        this.$message.success('Category was successfully updated');
                        this.back();
                    }).catch((error: AxiosError) => {
                        this.slugError();
                    });
                }
            },
        );
    }
    public back() {
        this.$router.push({
            name: `blog.categories.index`,
        });
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Categories',
            route: 'blog.categories.index',
        },
        {
            name: 'Edit category',
            route: 'blog.categories.edit',
        },
        ]);
    }

    public mounted() {
        this.getFormData();
        this.setBreadcrumbs();
    }
}
