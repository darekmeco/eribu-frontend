import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import TableMixin from '@/libs/mixins/TableMixin';

@Component({
    template: require("./Index.html"),
})
export default class Index extends Mixins(TableMixin) {

    public columns = [{
        title: 'Title',
        sorter: true,
        key: 'title',
        scopedSlots: {
            customRender: 'title',
        },
    },
    {
        title: 'Category',
        scopedSlots: {
            customRender: 'category',
        },
    },
    {
        title: "Created at",
        sorter: true,
        key: 'createdAt',
        scopedSlots: {
            customRender: 'createdAt',
        },
    },
    {
        title: "Actions",
        scopedSlots: {
            customRender: 'actions',
        },
    },
    ];

    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Posts',
            route: 'blog.posts.index',
        },
        ]);
    }

    public mounted() {
        this.setBreadcrumbs();
        // this.$store.commit('table/setPagination', {pageSize: 20});
    }
}
