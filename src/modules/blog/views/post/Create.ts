import {
    Component,
    Mixins,
} from 'vue-property-decorator';

import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import { Post } from '../../models/Post';
import SlugUnique from '@/libs/mixins/SlugUnique';

@Component({
    template: require("./Create.html"),
    components: {
        'a-editor': Editor,
    },
})
export default class Create extends Mixins( FormMixin, SlugUnique) {

    public formData: object = {
        title: ['title', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
        content: ['content', { rules: [{ required: true }] }],
        category: ['category', { rules: [{ required: true, message: 'Category is required' }] }],
        thumbnail: ['singleFiles.thumbnail'],
    };

    get categories() {
        return this.$store.state.blog.allCategories;
    }

    public submit() {
        this.form.validateFieldsAndScroll(
            (err: object, form: Post) => {
                if (!err) {
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/blog/posts/store`, form)
                    .then((response: AxiosResponse) => {
                        this.$message.success('Post was successfully added');
                        this.back();
                    }).catch((error: AxiosError) => {
                        this.slugError();
                    });
                }
            },
        );
    }

    public back() {
        this.$router.push({
            name: 'blog.posts.index',
        });
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Posts',
            route: 'blog.posts.index',
        },
        {
            name: 'Create post',
            route: 'blog.posts.create',
        },
        ]);
    }

    public mounted() {
        this.$store.dispatch('blog/allCategories');
        this.setBreadcrumbs();
    }
}
