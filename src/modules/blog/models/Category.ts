import File from '@/modules/media/models/File';

export interface Category {
    _id: string;
    name: string;
    slug: string;
    singleFiles: File[];
}
