import Layout from '@/views/Layout';
const routes = {
    path: '/blog',
    component: Layout,
    meta: { secure: true },
    children: [{
        path: 'posts/index',
        name: 'blog.posts.index',
        component: () => import( /* webpackChunkName: "about" */ './views/post/Index'),
    },
    {
        path: 'posts/create',
        name: 'blog.posts.create',
        component: () => import( /* webpackChunkName: "about" */ './views/post/Create'),
    },
    {
        path: 'posts/edit/:id',
        name: 'blog.posts.edit',
        component: () => import( /* webpackChunkName: "about" */ './views/post/Edit'),
    },
    {
        path: 'categories/index',
        name: 'blog.categories.index',
        component: () => import( /* webpackChunkName: "about" */ './views/category/Index'),
    },
    {
        path: 'categories/create',
        name: 'blog.categories.create',
        component: () => import( /* webpackChunkName: "about" */ './views/category/Create'),
    },
    {
        path: 'categories/edit/:id',
        name: 'blog.categories.edit',
        component: () => import( /* webpackChunkName: "about" */ './views/category/Edit'),
    },
    ],
};

export default routes;
