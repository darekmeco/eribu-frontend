import App from '@/App.vue';

const routes = {
  path: '/auth',
  component: App,
  children: [{
    path: 'login',
    name: 'login',
    component: () => import( /* webpackChunkName: "about" */ '@/modules/auth/views/Login'),
  },
  {
    path: 'register',
    name: 'register',
    component: () => import( /* webpackChunkName: "about" */ '@/modules/auth/views/Register'),
  },
  ],
};

export default routes;
