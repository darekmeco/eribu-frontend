import { Component, Vue } from "vue-property-decorator";
import MediaManager from "@/modules/media/components/MediaManager.vue";

@Component({
    template: require('./Index.html'),
    components: {
        MediaManager,
    },
})
export default class Files extends Vue {

    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [
            {
                name: "Deskop",
                route: "deskop",
            },
            {
                name: "Media",
                route: "media",
            },
        ]);
    }

    public mounted() {
        this.setBreadcrumbs();
    }
}
