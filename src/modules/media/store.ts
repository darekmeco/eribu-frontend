import _isArray from 'lodash/isArray';
import _findIndex from 'lodash/findIndex';
import _find from 'lodash/find';
import _union from 'lodash/union';
import _unionBy from 'lodash/unionBy';
import _get from 'lodash/get';
import _isEqual from 'lodash/isEqual';
import _remove from 'lodash/remove';
import File from '@/modules/media/models/File';
import { Module } from 'vuex';
import axios, { AxiosResponse } from 'axios';
import { PaginationInterface } from '@/libs/interfaces/PaginationInterface';
import _merge from 'lodash/merge';

interface MediaState {
    path: string;
    files: File[];
    paginationConfig: PaginationInterface;
    columns: object[];
}

const media: Module<MediaState, any> = {
    namespaced: true,
    state: {
        path: '',
        files: [],
        paginationConfig: {
            total: 100,
            current: 1,
            pageSize: 10,
            simple: false,
        },
        columns: [
            {
                title: 'Name',
                dataIndex: "filename",
                key: "filename",
                slots: { title: "customTitle" },
                scopedSlots: { customRender: "name" },
            },
            {
                title: "Miniaturka",
                key: "thumb",
                scopedSlots: { customRender: "thumb" },
            },
            {
                title: "Action",
                key: "action",
                scopedSlots: { customRender: "action" },
            },
        ],
    },
    mutations: {
        setPath(state: MediaState, path: string) {
            state.path = path;
        },
        setFiles(state: MediaState, files:File[]){
            state.files = files;
        },
        setPagination(state: MediaState, payload:any){
            state.paginationConfig = _merge(state.paginationConfig, payload)
        },
    },
    actions: {
        getFiles(context) {
            axios
                .post(`${process.env.VUE_APP_APIURL}/media/filesystem/files`, {
                    ...context.state.paginationConfig,
                    path: context.state.path
                })
                .then((res: AxiosResponse) => {
                    context.commit('setPagination', {total: res.data.total});
                    context.commit('setFiles', res.data.files);
                });
        }
    },
    getters: {
        path: state => state.path,
        pagination: state => state.paginationConfig,
        files: state => state.files,
    },
};

export default media;
