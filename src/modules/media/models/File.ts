export default interface File {
    filename?: string;
    description?: string;
    path?: string;
    kind?: string;
}

// export default class File {
//     private kind: string;
//     constructor(
//         private filename: string, 
//         private key: string, 
//         private path: string, 
//         private searchpath: string) { }
// }