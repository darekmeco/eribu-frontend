import {
    Component,
    Mixins,
    Prop,
    Emit,
} from "vue-property-decorator";
import Dialogs from '../../../libs/mixins/Dialogs';
import FileThumbLink from "@/modules/media/components/FileThumbLink.vue";
import {
    Modal,
} from "ant-design-vue";
import {
    AxiosResponse,
    AxiosError,
} from 'axios';
import File from '../models/File';
@Component({
    template: require('./MediaTable.html'),
    components: {
        'a-modal': Modal,
        FileThumbLink,
    },
})
export default class MediaTable extends Mixins(Dialogs) {
    public $axios: any;

    @Prop({ default: false }) pickFile: boolean;

    get columns() {
        return this.$store.state.media.columns;
    }
    get path() {
        return this.$store.getters['media/path'];
    }
    get pagination() {
        return this.$store.getters['media/pagination'];
    }
    get files() {
        return this.$store.getters['media/files'];
    }

    public handleTableChange(pagination: any, filters: any, sorter: any) {
        this.$store.commit('media/setPagination', pagination);
    }
    @Emit('file-inserted')
    private insertFile(file: File) {
        return file;
    }
    public async deleteFile(row: object) {
        this.confirmCustomDelete(() => {
            this.$axios
                .delete(`${process.env.VUE_APP_APIURL}/media/filesystem/files`, {
                    data: row,
                })
                .then((res: AxiosResponse) => {
                    if (res.data.status === "success") {
                        this.$store.dispatch('media/getFiles');
                    }
                })
                .catch((error: AxiosError) => {
                    console.log(error);
                });
        });
    }


    public mounted() {
        this.$store.dispatch('media/getFiles');
    }
}
