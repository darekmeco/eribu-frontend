import {
  Component,
  Mixins,
} from 'vue-property-decorator';
import TableMixin from '@/libs/mixins/TableMixin';
import FormatPrice from '@/libs/mixins/FormatPrice';

@Component({
  template: require("./Index.html"),
})
export default class Index extends Mixins(TableMixin, FormatPrice) {

  public columns = [{
    title: "Name",
    scopedSlots: {
      customRender: "name",
    },
    sorter: true,
    key: 'name',
  },
  {
    title: "Price",
    dataIndex: "price",
    key: "price",
    scopedSlots: {
      customRender: "price",
    },
    sorter: (a: any, b: any) => a.price - b.price,
  },
  {
    title: "Created at",
    dataIndex: "createdAt",
    key: "createdAt",
    scopedSlots: {
      customRender: "createdAt",
    },
    sorter: true,
  },
  {
    title: "Actions",
    scopedSlots: {
      customRender: 'actions',
    },
  },
  ];

  public setBreadcrumbs() {
    this.$store.commit('setBreadcrumbs', [{
      name: "Deskop",
      route: "deskop",
    },
    {
      name: "Products",
      route: null,
    },
    ]);
  }

  public mounted() {
    this.setBreadcrumbs();
  }
}
