import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import { Product } from '../../models/Product';
import _merge from 'lodash/merge';
import { InputNumber, Tree } from 'ant-design-vue';
import SlugUnique from '@/libs/mixins/SlugUnique';
import FileChooser from '@/modules/media/components/FileChooser';
@Component({
    template: require("./Edit.html"),
    components: {
        'a-editor': Editor,
        'a-input-number': InputNumber,
        "a-tree": Tree,
        FileChooser,
    },
})
export default class Edit extends Mixins(FormMixin, SlugUnique) {

    public selectedCategories: string[] = [];
    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
        description: ['description', { rules: [{ required: true }] }],
        price: ['price', { rules: [{ required: true, type: 'number' }] }],
        categories: ['categories'],
        status: ['status'],
        thumbnail: ['singleFiles.thumbnail'],
        gallery: ['multipleFiles.gallery'],
    };

    get categories() {
        return this.$store.state.product.categoryList;
    }

    public getFormData() {
        this.$axios.get(`${process.env.VUE_APP_APIURL}/product/products/edit`, {
            params: {
                id: this.$route.params.id,
            },
        }).then((res: any) => {
            this.setDataFields(this.form.getFieldsValue(), res.data.model);
            this.selectedCategories = res.data.model.categories;
        });
    }
    public categoryCheck(value: any) {
        this.form.setFieldsValue({
            categories: value.checked,
        });
    }
    public back() {
        this.$router.push({
            name: "product.products.index",
        });
    }
    public submit() {
        this.form.validateFieldsAndScroll((err: any, form: Product) => {
            if (!err) {
                form = _merge(form, { _id: this.$route.params.id });
                this.$axios.post(`${process.env.VUE_APP_APIURL}/product/products/update`, form)
                .then((response: AxiosResponse) => {
                    this.$message.success('Product was successfully updated');
                    this.back();
                }).catch((error: AxiosError) => {
                    this.slugError();
                });
            }
        });
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [
            {
                name: "Deskop",
                route: "deskop",
            },
            {
                name: "Products",
                route: "product.products.index",
            },
            {
                name: "Edit product",
                route: null,
            },
        ]);
    }

    public mounted() {
        this.$store.dispatch('product/getCategoryList');
        this.getFormData();
        this.setBreadcrumbs();
    }
}
