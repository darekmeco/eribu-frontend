import {
  Component,
  Mixins,
} from 'vue-property-decorator';
import Breadcrumb from "@/components/Breadcrumb.vue";
import {
  Tree,
  Alert,
} from "ant-design-vue";
import _isEmpty from "lodash/fp/isEmpty";
import _head from "lodash/fp/head";
import _getOr from "lodash/fp/getOr";
import { Category } from '../../models/Category';
import Dialogs from '@/libs/mixins/Dialogs';

@Component({
  template: require("./Index.html"),
  components: {
    Breadcrumb,
    "a-tree": Tree,
    "a-alert": Alert,
  },
})
export default class Index extends Mixins(Dialogs) {
  public createModal: boolean = false;
  public $axios: any;
  public defaultExpandAll: boolean = true;

  public data() {
    return {
      form: this.$form.createForm(this),
    };
  }
  /**
   * Is any node selected
   * @return boolean
   */
  get nodeSelected() {
    return !_isEmpty(this.currentNode);
  }
  get categoryList(): Category[] {
    return this.$store.state.product.categoryList;
  }
  /**
   * Currently selected node
   * @return any
   */
  get currentNode(): any {
    return _head(this.$store.state.product.selectedNodes);
  }
  get selectedNodes(): string[] {
    return this.$store.state.product.selectedNodes;
  }

  public onDrop(info: any) {
    this.confirmCustomMove(() => {
      this.$store.dispatch('product/nodeMove', info);
    });
  }
  public onSelect(selectedKeys: Category[], info: any) {
    const node = _head(selectedKeys);
    if (node) {
      this.$store.commit('product/setSelectedNodes', node);
      this.$router.push({
        name: 'product.categories.edit',
        params: { id: this.currentNode },
      });
    }
  }
  public async deleteCategory(withChildren = false) {
    this.confirmCustomDelete(() => {
      const deleteCategory = this.$store.dispatch('product/deleteCategory', withChildren);
      deleteCategory.then(() => {
        this.$message.success('Category successfully deleted');
      });
    });
  }
  public setBreadcrumbs() {
    this.$store.commit('setBreadcrumbs', [
      {
        name: "Deskop",
        route: "deskop",
      },
      {
        name: "Product categories",
        route: null,
      },
    ]);
  }

  public mounted() {
    this.$store.dispatch('product/getCategoryList');
    this.setBreadcrumbs();
  }
}
