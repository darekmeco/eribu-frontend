import File from '@/modules/media/models/File';

export interface Product {
    _id: string;
    name: string;
    slug: string;
    description: string;
    price: number;
    categories: number[];
    status: number;
    singleFiles: File[];
}
