import _isArray from 'lodash/isArray.js';
import _findIndex from 'lodash/findIndex.js';
import _getOr from "lodash/fp/getOr";
import _head from "lodash/fp/head";
import axios, { AxiosResponse, AxiosError } from 'axios';
import { Category } from './models/Category';
import { Module } from 'vuex';

interface ProductState {
  categoryList: Category[];
  selectedNodes: any[];
}

const product: Module<ProductState, any> = {
  namespaced: true,
  state: {
    categoryList: [],
    selectedNodes: [],
  },
  mutations: {
    setCategoryList(state: ProductState, items: Category[]) {
      state.categoryList = items;
    },
    setSelectedNodes(state: ProductState, item: string) {
      state.selectedNodes = [item];
    },
  },
  actions: {
    getCategoryList(context: any) {
      axios.get(`${process.env.VUE_APP_APIURL}/product/categories/tree`)
        .then((res: AxiosResponse) => {
          context.commit('setCategoryList', res.data);
          if (context.state.selectedNodes.length === 0) {
            const rootCategory: any = _head(context.state.categoryList);
            context.commit('setSelectedNodes', rootCategory._id);
          }
        }).catch((error: AxiosError) => {
          console.log(error);
        });
    },
    deleteCategory(context: any, withChildren: boolean) {
      axios.delete(`${process.env.VUE_APP_APIURL}/product/categories/delete`, {
        data: {
          id: _head(context.state.selectedNodes),
          all: withChildren,
        },
      }).then((res: AxiosResponse) => {
        context.commit('setCategoryList', res.data);
        const rootCategory: any = _head(context.state.categoryList);
        context.commit('setSelectedNodes', rootCategory._id);
      }).catch((error: AxiosError) => {
        console.log(error);
      });
    },
    nodeMove(context: any, info: any) {
      const data: any = {};
      data.dropKey = info.node.eventKey;
      data.dragKey = info.dragNode.eventKey;
      data.dropPos = info.node.pos.split("-");
      data.dropPosition = info.dropPosition - Number(data.dropPos[data.dropPos.length - 1]);
      axios
        .post(`${process.env.VUE_APP_APIURL}/product/categories/move`, data)
        .then((res: AxiosResponse) => {
          context.commit('setCategoryList', res.data);
        })
        .catch((error: any) => {
          console.log(error, 'error');
        });
    },
  },
};
export default product;
