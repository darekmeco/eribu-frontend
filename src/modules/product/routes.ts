import Layout from '@/views/Layout';
import EmptyRouter from '@/views/EmptyRouter.vue';

const routes = {
  path: '/product',
  meta: { secure: true },
  component: Layout,
  children: [
    {
      path: 'products',
      component: EmptyRouter,
      children: [
        {
          path: 'index',
          name: 'product.products.index',
          component: () => import( /* webpackChunkName: "about" */ '@/modules/product/views/product/Index'),
        },
        {
          path: 'create',
          name: 'product.products.create',
          component: () => import( /* webpackChunkName: "about" */ '@/modules/product/views/product/Create'),
        },
        {
          path: 'edit/:id',
          name: 'product.products.edit',
          component: () => import( /* webpackChunkName: "about" */ '@/modules/product/views/product/Edit'),
        },
      ],
    },
    {
      path: 'categories',
      component: EmptyRouter,
      children: [
        {
          path: 'index',
          name: 'product.categories.index',
          component: () => import( /* webpackChunkName: "about" */ '@/modules/product/views/category/Index'),
          children: [
            {
              path: 'create',
              name: 'product.categories.create',
              component: () => import( /* webpackChunkName: "about" */ '@/modules/product/views/category/Create'),
            },
            {
              path: 'edit/:id',
              name: 'product.categories.edit',
              component: () => import( /* webpackChunkName: "about" */ '@/modules/product/views/category/Edit'),
            },
          ],
        },
      ],
    },
  ],
};

export default routes;
