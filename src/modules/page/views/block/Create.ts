import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import { Block } from '../../models/Block';
import { AxiosResponse, AxiosError } from 'axios';
import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import SlugUnique from '@/libs/mixins/SlugUnique';
import FileChooser from '@/modules/media/components/FileChooser';

@Component({
    template: require("./Create.html"),
    components: {
        'a-editor': Editor,
        FileChooser: FileChooser,
    },

})
export default class Create extends Mixins(FormMixin, SlugUnique) {

    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
        content: ['content'],
        short_content: ['short_content'],
        thumbnail: ['singleFiles.thumbnail'],
        main: ['singleFiles.main'],
        gallery: ['multipleFiles.gallery'],
    };

    public submit() {
        this.form.validateFieldsAndScroll(
            (err: object, form: Block) => {
                if (!err) {
                    console.log(form, 'form');
                    
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/page/blocks/store`, form)
                        .then((response: AxiosResponse) => {
                            this.$message.success('Block was successfully added');
                            this.back();
                        }).catch((error: AxiosError) => {
                            this.slugError();
                        });
                }
            },
        );
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Blocks',
            route: `page.blocks.index`,
        },
        {
            name: `Create block`,
            route: null,
        },
        ]);
    }
    public back() {
        this.$router.push({
            name: `page.blocks.index`,
        });
    }

    public mounted() {
        this.setBreadcrumbs();
    }
}
