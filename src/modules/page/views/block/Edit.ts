import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import { Block } from '../../models/Block';
import { AxiosResponse, AxiosError } from 'axios';
import _merge from 'lodash/merge';
import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import SlugUnique from '@/libs/mixins/SlugUnique';
import FileChooser from '@/modules/media/components/FileChooser';

@Component({
    template: require("./Edit.html"),
    components: {
        'a-editor': Editor,
        'file-chooser': FileChooser,
    },
})
export default class Edit extends Mixins(FormMixin, SlugUnique) {

    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
        content: ['content'],
        short_content: ['short_content'],
        thumbnail: ['singleFiles.thumbnail'],
        gallery: ['multipleFiles.gallery'],

    };

    public getFormData() {
        this.$axios.get(`${process.env.VUE_APP_APIURL}/page/blocks/edit`, {
            params: {
                id: this.$route.params.id,
            },
        }).then((res: AxiosResponse) => {
            this.setDataFields(this.form.getFieldsValue(), res.data.model);
        }).catch((error: AxiosError) => {
            console.log(error);
        });
    }
    public submit() {
        this.form.validateFieldsAndScroll(
            (err: any, form: Block) => {
                if (!err) {
                    form = _merge(form, { _id: this.$route.params.id });
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/page/blocks/update`, form)
                    .then((response: AxiosResponse) => {
                        this.$message.success('Block was successfully added');
                        this.back();
                    }).catch((error: AxiosError) => {
                        this.slugError();
                    });
                }
            },
        );
    }
    public back() {
        this.$router.push({
            name: `page.blocks.index`,
        });
    }

    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Blocks',
            route: 'page.blocks.index',
        },
        {
            name: 'Edit block',
            route: 'page.blocks.edit',
        },
        ]);
    }

    public mounted() {
        this.getFormData();
        this.setBreadcrumbs();
    }
}
