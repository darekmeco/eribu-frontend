import {
    Component,
    Mixins,
  } from 'vue-property-decorator';
import _merge from 'lodash/merge';
import { AxiosResponse, AxiosError } from 'axios';
import { Page } from '../../models/Page';
import Editor from '@/components/form/Editor.vue';
import { Collapse } from "ant-design-vue";
import FormMixin from '@/libs/mixins/FormMixin';
import SlugUnique from '@/libs/mixins/SlugUnique';
import FileChooser from '@/modules/media/components/FileChooser';
@Component({
    template: require("./Edit.html"),
    components: {
        'a-editor': Editor,
        "a-collapse": Collapse,
        "a-collapse-panel": Collapse.Panel,
        'file-chooser': FileChooser,
    },
})
export default class Edit extends Mixins(FormMixin, SlugUnique) {

    public breadcrumbs = [{
        name: 'Deskop',
        route: "deskop",
    },
    {
        name: 'Pages',
        route: 'page.pages.index',
    },
    {
        name: 'Edit page',
        route: null,
    },
    ];

    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
        content: ['content', { rules: [{ required: true }] }],
        meta_title: ['meta_title'],
        meta_description: ['meta_description'],
        og_title: ['og_title'],
        og_description: ['og_description'],
        og_type: ['og_type'],
        thumbnail: ['singleFiles.thumbnail'],
        gallery: ['multipleFiles.gallery'],
    };

    public getFormData() {
        this.$axios.get(`${process.env.VUE_APP_APIURL}/page/pages/edit`, {
            params: {
                id: this.$route.params.id,
            },
        }).then((res: any) => {
            this.setDataFields(this.form.getFieldsValue(), res.data.model);
        });
    }
    public submit() {
        this.form.validateFieldsAndScroll(
            (err: any, form: Page) => {
                if (!err) {
                    form = _merge(form, { _id: this.$route.params.id });
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/page/pages/update`, form)
                    .then((response: AxiosResponse) => {
                        this.$message.success('Page was successfully updated');
                        this.back();
                    }).catch((error: AxiosError) => {
                        this.slugError();
                    });
                }
            },
        );
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Pages',
            route: 'page.pages.index',
        },
        {
            name: 'Edit page',
            route: null,
        },
        ]);
    }
    public back() {
        this.$router.push({
            name: 'page.pages.index',
        });
    }

    public mounted() {
        this.getFormData();
        this.setBreadcrumbs();
    }
}
