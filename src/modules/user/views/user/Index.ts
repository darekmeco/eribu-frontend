import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import TableMixin from '@/libs/mixins/TableMixin';

@Component({
    template: require("./Index.html"),
})
export default class Index extends Mixins(TableMixin) {

    public columns = [{
        title: 'Name',
        sorter: true,
        key: 'firstname',
        scopedSlots: {
            customRender: 'name',
        },
    }, {
        title: 'Email',
        sorter: true,
        key: 'email',
        scopedSlots: {
            customRender: 'email',
        },
    }, {
        title: "Created at",
        sorter: true,
        key: 'createdAt',
        scopedSlots: {
            customRender: 'createdAt',
        },
    }, {
        title: "Actions",
        scopedSlots: {
            customRender: 'actions',
        },
    },
    ];

    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Users',
            route: 'user.users.index',
        },
        ]);
    }

    public mounted() {
        this.setBreadcrumbs();
        // this.$store.commit('table/setPagination', {pageSize: 20});
    }
}
