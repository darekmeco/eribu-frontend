import {
    Component,
    Mixins,
} from 'vue-property-decorator';
import { Role } from '../../models/Role';
import { AxiosResponse, AxiosError } from 'axios';
import FormMixin from '@/libs/mixins/FormMixin';
import _merge from 'lodash/merge';
import SlugUnique from '@/libs/mixins/SlugUnique';
import Permission from '../../components/Permission.vue';
import { Tabs } from 'ant-design-vue';

@Component({
    template: require("./Edit.html"),
    components: {
        'a-tabs': Tabs,
        'a-tab-pane': Tabs.TabPane,
        'permission': Permission,
    },
})
export default class Edit extends Mixins(FormMixin, SlugUnique) {

    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
        permissions: ['permissions'],
    };

    public getFormData() {
        this.$axios.get(`${process.env.VUE_APP_APIURL}/user/roles/edit`, {
            params: {
                id: this.$route.params.id,
            },
        }).then((res: any) => {
            this.setDataFields(this.form.getFieldsValue(), res.data.model);
        });
    }
    public submit() {
        this.form.validateFieldsAndScroll(
            (err: any, form: Role) => {
                if (!err) {
                    form = _merge(form, {_id: this.$route.params.id});
                    this.$axios.post(`${process.env.VUE_APP_APIURL}/user/roles/update`, form)
                    .then((response: AxiosResponse) => {
                        this.$message.success('Role was successfully updated');
                        this.back();
                    }).catch((error: AxiosError) => {
                        this.slugError();
                    });
                }
            },
        );
    }
    public back() {
        this.$router.push({
            name: `user.roles.index`,
        });
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [{
            name: 'Deskop',
            route: "deskop",
        },
        {
            name: 'Roles',
            route: 'user.roles.index',
        },
        {
            name: 'Edit role',
            route: 'user.roles.edit',
        },
        ]);
    }

    public mounted() {
        this.getFormData();
        this.setBreadcrumbs();
    }
}
