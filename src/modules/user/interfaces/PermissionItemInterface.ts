export default interface PermissionItemInterface {
    parent: string;
    action: string;
    value: string;
}
