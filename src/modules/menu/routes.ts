import Layout from '@/views/Layout';
import EmptyRouter from '@/views/EmptyRouter.vue';

const routes = {
  path: '/menus',
  meta: { secure: true },
  component: Layout,
  children: [
    {
      path: 'menu',
      component: EmptyRouter,
      children: [
        {
          path: 'index',
          name: 'menu.root.index',
          component: () => import( /* webpackChunkName: "about" */ '@/modules/menu/views/root/Index'),
        },
        {
          path: 'create',
          name: 'menu.root.create',
          component: () => import( /* webpackChunkName: "about" */ '@/modules/menu/views/root/Create'),
        },
        {
          path: 'edit/:id',
          name: 'menu.root.edit',
          component: () => import( /* webpackChunkName: "about" */ '@/modules/menu/views/root/MenuTree'),
          children: [
            {
              path: 'menu-item',
              name: 'menu.child.create',
              component: () => import( /* webpackChunkName: "about" */ '@/modules/menu/views/menu-item/Create'),
            },
            {
              path: 'menu-item/:childId',
              name: 'menu.child.edit',
              component: () => import( /* webpackChunkName: "about" */ '@/modules/menu/views/menu-item/Edit'),
            },
          ],
        },
      ],
    },
  ],
};

export default routes;
