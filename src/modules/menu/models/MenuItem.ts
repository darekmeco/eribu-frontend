export interface MenuItem {
    _id: string;
    name: string;
    slug: string;
}
