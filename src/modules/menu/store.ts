import _isArray from 'lodash/isArray.js';
import _findIndex from 'lodash/findIndex.js';
import _getOr from "lodash/fp/getOr";
import _head from "lodash/fp/head";
import axios, { AxiosResponse, AxiosError } from 'axios';
import { MenuItem } from './models/MenuItem';
import { Module } from 'vuex';

interface MenuState {
    selectedNodes: any[];
    nodeTree: MenuItem[];
}

const product: Module<MenuState, any> = {
    namespaced: true,
    state: {
        selectedNodes: [],
        nodeTree: [],
    },
    mutations: {
        setNodeTree(state: MenuState, tree: any) {
            state.nodeTree = tree;
        },
        setSelectedNodes(state: MenuState, item: string) {
            state.selectedNodes = [item];
        },
    },
    actions: {
        getNodeTree(context: any, id: string) {
            axios.get(`${process.env.VUE_APP_APIURL}/menus/menu/edit`, {
                params: {
                    id,
                },
            }).then((res: any) => {
                context.commit('setNodeTree', res.data.tree);
                context.commit('setSelectedNodes', id);
            });
        },
        deleteMenuItem(context: any, withChildren: boolean) {
            axios.delete(`${process.env.VUE_APP_APIURL}/menus/item/delete`, {
                data: {
                    id: _head(context.state.selectedNodes),
                    all: withChildren,
                },
            }).then((res: AxiosResponse) => {
                context.commit('setMenuItemList', res.data);
                const rootMenuItem: any = _head(context.state.menuItemList);
                context.commit('setSelectedNodes', rootMenuItem._id);
            }).catch((error: AxiosError) => {
                console.log(error);
            });
        },
        nodeMove(context: any, info: any) {
            const data: any = {};
            data.dropKey = info.node.eventKey;
            data.dragKey = info.dragNode.eventKey;
            data.dropPos = info.node.pos.split("-");
            data.dropPosition = info.dropPosition - Number(data.dropPos[data.dropPos.length - 1]);
            axios
                .post(`${process.env.VUE_APP_APIURL}/menus/item/move`, data)
                .then((res: AxiosResponse) => {
                    context.commit('setMenuItemList', res.data);
                })
                .catch((error: any) => {
                    console.log(error, 'error');
                });
        },
    },
};
export default product;
