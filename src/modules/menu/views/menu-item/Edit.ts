import {
    Component,
    Mixins,
    Watch,
} from 'vue-property-decorator';

import Editor from '@/components/form/Editor.vue';
import FormMixin from '@/libs/mixins/FormMixin';
import { AxiosError, AxiosResponse } from 'axios';
import _merge from 'lodash/merge';
import SlugUnique from '@/libs/mixins/SlugUnique';

@Component({
    template: require("./Edit.html"),
})
export default class Edit extends Mixins( FormMixin, SlugUnique) {

    public formData: object = {
        name: ['name', { rules: [{ required: true }] }],
        slug: ['slug', { rules: [{ validator: this.validateSlug }] }],
        thumbnail: ['singleFiles.thumbnail'],
    };

    public getFormData() {
        this.$axios.get(`${process.env.VUE_APP_APIURL}/menus/menu/edit`, {
            params: {
                id: this.$route.params.childId,
            },
        }).then((res: any) => {
            console.log(res, 'response');
            this.setDataFields(this.form.getFieldsValue(), res.data.model);
            console.log(res.data, 'res.data');
        });
    }
    public back() {
        this.$router.push({
            name: "menu.root.index",
        });
    }
    public submit() {
        this.form.validateFieldsAndScroll((err: any, form: any) => {
            if (!err) {
                form = _merge(form, { _id: this.$route.params.childId });
                this.$axios.post(`${process.env.VUE_APP_APIURL}/menus/menu/update`, form)
                    .then((response: AxiosResponse) => {
                        this.$message.success('Category was successfully updated');
                        this.$store.dispatch('menu/getNodeTree', this.$route.params.id);
                    }).catch((error: AxiosError) => {
                        console.log(error, 'error');
                        this.slugError();
                    });
            }
        });
    }
    public setBreadcrumbs() {
        this.$store.commit('setBreadcrumbs', [
          {
            name: "Deskop",
            route: "deskop",
          },
          {
            name: "Menu",
            route: "menu.root.index",
          },
          {
            name: "Edit menu",
            route: null,
          },
        ]);
      }
    @Watch('$route')
    public onchange(to, from) {
        this.getFormData();
    }
    public mounted() {
        this.getFormData();
        this.setBreadcrumbs();
    }
}
