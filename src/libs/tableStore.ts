import { Module } from 'vuex';
import axios from 'axios';
import _merge from 'lodash/merge';
import { PaginationInterface } from '@/libs/interfaces/PaginationInterface';

interface TableState {
    items: any[];
    loading: boolean;
    pagination: PaginationInterface;
}

const store: Module<TableState, any> = {
    namespaced: true,
    state: {
        items: [],
        loading: false,
        pagination: {
            pageSize: 10,
            current: 1,
            searchInput: '',
            total: 10,
        },
    },
    mutations: {
        setItems(state, items: any) {
            state.items = items;
        },
        setLoading(state: TableState, value: boolean) {
            state.loading = value;
        },
        setPagination(state: TableState, value: PaginationInterface) {
            state.pagination = _merge(state.pagination, value);
        },
    },
    actions: {
        getItems(context: any) {
            context.commit('setLoading', true);
            axios.get(`${process.env.VUE_APP_APIURL}${location.pathname}`, {
                params: context.state.pagination,
            }).then((response) => {
                console.log('getItems: ', response.data.results);
                context.commit('setItems', response.data.results);
                context.commit('setPagination', response.data.pagination);
                context.commit('setLoading', false);
            }).catch((error) => {
                context.commit('setLoading', false);
            });
        },

    },
    getters: {
        items(state) {
            return state.items;
        },
    },
};

export default store;
