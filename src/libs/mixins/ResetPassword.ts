import { Vue, Component } from 'vue-property-decorator';

@Component
export default class ResetPassword extends Vue {

    public resetPassForm: any;
    public confirmDirty: boolean = false;

    public handleConfirmBlur(e: any) {
        const value = e.target.value;
        this.confirmDirty = this.confirmDirty || !!value;
    }
    public compareToFirstPassword(rule: any, value: any, callback: any) {
        const resetPassForm = this.resetPassForm;
        if (value && value !== resetPassForm.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    }
    public validateToNextPassword(rule: any, value: any, callback: any) {
        const resetPassForm = this.resetPassForm;
        if (value && this.confirmDirty) {
            resetPassForm.validateFields(['password_confirm'], {
                force: true,
            });
        }
        callback();
    }
}
