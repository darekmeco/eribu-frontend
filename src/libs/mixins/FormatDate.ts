import Vue from 'vue';
import Component from 'vue-class-component';
import moment from 'moment';

@Component({
    filters: {
        formatDate(value: any) {
            return moment(value).format('DD-MM-YYYY HH:mm:ss');
        },
    },
})
export default class FormatDate extends Vue {}
