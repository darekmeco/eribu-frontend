import axios, { AxiosInstance, AxiosRequestConfig } from "axios";

class Axios {
  public axiosInstance: AxiosInstance;
  public config = {
    // baseURL: process.env.baseURL || process.env.apiUrl || ""
    // timeout: 60 * 1000, // Timeout
    // withCredentials: true, // Check cross-site Access-Control
  };

  constructor() {
    this.updateAuthToken();
    this.axiosInstance = axios.create(this.config);
  }
  public updateAuthToken() {
    const token = localStorage.getItem('jwt');
    if (token) {
      axios.defaults.headers.common.Authorization = 'Bearer ' + token;
    }
    // axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
    // axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
  }
  public getAxiosInstance() {
    return this.axiosInstance;
  }
  public get(url: string, config?: AxiosRequestConfig) {
    return this.axiosInstance.get(url, config);
  }
  public post(url: string, data?: any, config?: AxiosRequestConfig) {
    return this.axiosInstance.post(url, data, config);
  }
  public delete(url: string, data?: any, config?: AxiosRequestConfig) {
    return this.axiosInstance.delete(url, data);
  }
  public hooks() {
    this.axiosInstance.interceptors.request.use(
      (config) => {
        // Do something before request is sent
        return config;
      },
      (error) => {
        // Do something with request error
        return Promise.reject(error);
      },
    );

    // Add a response interceptor
    this.axiosInstance.interceptors.response.use(
      (response) => {
        // Do something with response data
        return response;
      },
      (error) => {
        // Do something with response error
        return Promise.reject(error);
      },
    );
  }
}

export default Axios;
