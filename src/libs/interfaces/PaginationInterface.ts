export interface PaginationInterface {
    pageSize?: number;
    current?: number;
    searchInput?: string;
    total?: number;
    simple?: boolean;
}
