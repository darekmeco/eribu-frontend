import Vue from 'vue';
import Router, { Route } from 'vue-router';
import Layout from './views/Layout';
import Dashboard from './views/Dashboard';

import productRoutes from './modules/product/routes';
import mediaRoutes from './modules/media/routes';
import userRoutes from './modules/user/routes';
import authRoutes from './modules/auth/routes';
import pageRoutes from './modules/page/routes';
import blogRoutes from './modules/blog/routes';
import menuRoutes from './modules/menu/routes';

import store from './store';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Layout,
      children: [{
        path: '/',
        component: Dashboard,
        name: 'deskop',
        meta: { secure: true },
      }],
    },
    mediaRoutes,
    productRoutes,
    userRoutes,
    authRoutes,
    pageRoutes,
    blogRoutes,
    menuRoutes,
  ],
});

router.beforeEach((to: Route, from: Route, next) => {

  if (to.matched.some((record) => record.meta.secure)) {
    if (localStorage.getItem('jwt') == null) {
      next({
        path: '/auth/login',
        params: { nextUrl: to.fullPath },
      });
    } else {
      const user = JSON.parse(localStorage.getItem('user') as string);
      if (to.matched.some((record) => record.meta.is_admin)) {
        if (user.is_admin === 1) {
          next();
        } else {
          next({ name: 'dashboard' });
        }
      } else {
        next();
      }

    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (localStorage.getItem('jwt') == null) {
      next();
    } else {
      next({ name: 'dashboard' });
    }
  } else {
    next();
  }
});
export default router;
