import Vue from "vue";
import Component from "vue-class-component";

@Component({
  template: require("./Dashboard.html"),
})
export default class Dashboard extends Vue {

  get fullname() {
    return this.$store.getters.fullName;
  }

  public setBreadcrumbs() {
      this.$store.commit('setBreadcrumbs', [{
        name: 'Deskop',
        route: "deskop",
      },
    ]);
  }
  public mounted() {
    this.setBreadcrumbs();
  }
}
