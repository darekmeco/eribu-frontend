
import { Component, Vue } from "vue-property-decorator";

import Sidebar from "@/components/Sidebar.vue";
import UserMenu from "@/components/UserMenu.vue";
import Breadcrumbs from '@/components/Breadcrumb.vue';
import '@/assets/styles/main.less';
import '@/libs/antComponents';
@Component({
  template: require('./Layout.html'),
  components: {
    Sidebar,
    UserMenu,
    Breadcrumbs,
  },
})

export default class Layout extends Vue {
  public $axios: any;
  public collapsed = false;
  public theme: string = 'dark';
  public created() {
    if (this.$route.name) {
      const openKeys = this.$route.name.split('.');
      this.$store.commit('setSidebarOpenKeys', openKeys.length > 1 ? openKeys : []);
      this.$store.commit('setSidebarSelectedKeys', [this.$route.name]);
    }
  }
  public changeTheme(checked: boolean) {
    this.theme = checked ? 'light' : 'dark';
  }

  public mounted() {
    this.$store.dispatch('getCurrentUser');
  }
}
