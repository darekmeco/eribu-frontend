module.exports = {
    runtimeCompiler: true,
    configureWebpack: {
    module: {
      rules: [{
        test: /\.html$/,
        use: 'vue-html-loader'
      }]
    },
    plugins: [
      
    ]
  }
}
